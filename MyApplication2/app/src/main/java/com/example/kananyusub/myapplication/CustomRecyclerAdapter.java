package com.example.kananyusub.myapplication;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.kananyusub.myapplication.models.Person;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CustomRecyclerAdapter extends RecyclerView.Adapter {


    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_PERSON = 1;
    private static final int VIEW_TYPE_PERSON_WITH_GENDER = 2;

    private boolean isLoaderVisible = false;

    private Context mContext;
    private List<Person> mPersonList;

    public CustomRecyclerAdapter(Context context, List<Person> personlist) {
        mContext = context;
        mPersonList = personlist;
    }

    @Override
    public int getItemViewType(int position) {
        Log.e("ERROR",mPersonList.get(position).isGenderVisible()+"");
        if (position == mPersonList.size() - 1 && isLoaderVisible) {
            return VIEW_TYPE_LOADING;
        } else if (mPersonList.get(position).isGenderVisible()) {
            return VIEW_TYPE_PERSON_WITH_GENDER;
        } else {
            return VIEW_TYPE_PERSON;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEW_TYPE_PERSON:
                view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_person, parent, false);
                return new PersonViewHolder(view);
            case VIEW_TYPE_PERSON_WITH_GENDER:
                view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_person_with_gender, parent, false);
                return new PersonWithGenderViewHolder(view);
            case VIEW_TYPE_LOADING:
                view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_loading, parent, false);
                return new FooterHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case VIEW_TYPE_PERSON:
                PersonViewHolder personViewHolder = (PersonViewHolder) holder;
                Glide.with(mContext)
                        .asBitmap()
                        .load(mPersonList.get(position).getImageId())
                        .into(personViewHolder.mImageViewPerson);

                personViewHolder.mTextViewFullName.setText(mPersonList.get(position).getFullName());
                personViewHolder.mTextViewAge.setText(mPersonList.get(position).getAge());
                break;

            case VIEW_TYPE_PERSON_WITH_GENDER:
                PersonWithGenderViewHolder personWithGenderViewHolder = (PersonWithGenderViewHolder) holder;

                Glide.with(mContext)
                        .asBitmap()
                        .load(mPersonList.get(position).getImageId())
                        .into(personWithGenderViewHolder.mImageViewPerson);

                personWithGenderViewHolder.mTextViewFullName.setText(mPersonList.get(position).getFullName());
                personWithGenderViewHolder.mTextViewAge.setText(mPersonList.get(position).getAge());
                personWithGenderViewHolder.mTextViewGender.setText(mPersonList.get(position).getGender());
                break;

            case 0:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mPersonList == null ? 0 : mPersonList.size();
    }

    public void add(Person person) {
        mPersonList.add(person);
        notifyItemInserted(mPersonList.size() - 1);
    }

    public void addAll(List<Person> personList) {
        for (Person p : personList) {
            add(p);
        }
    }

    private void remove(Person person) {
        int position = mPersonList.indexOf(person);
        if (position > -1) {
            mPersonList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addLoading() {
        isLoaderVisible = true;
        add(new Person());
    }

    public void removeLoading() {
        isLoaderVisible = false;
        int position = mPersonList.size() - 1;
        Person person = getItem(position);

        if (person != null) {
            mPersonList.remove(position);
            notifyItemRemoved(position);
        }
    }

    Person getItem(int position) {
        return mPersonList.get(position);
    }
}
