package com.example.kananyusub.myapplication.utils;

import com.example.kananyusub.myapplication.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ImageProvider {

    private static int mPersonIndex = 0;


    private static List<Integer> mPersonImageIds = new ArrayList<Integer>(){
        {
            add(R.drawable.man);
            add(R.drawable.man);
            add(R.drawable.man);
            add(R.drawable.man);
            add(R.drawable.man);
            add(R.drawable.man);
            add(R.drawable.man);
            add(R.drawable.man);
            add(R.drawable.man);
            add(R.drawable.man);
        }
    };

    public static Integer getNextPersonId(){
        if (mPersonIndex == mPersonImageIds.size()) mPersonIndex = 0;
        return mPersonImageIds.get(mPersonIndex++);
    }

}
