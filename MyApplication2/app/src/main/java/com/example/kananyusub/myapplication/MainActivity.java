package com.example.kananyusub.myapplication;

import android.os.Bundle;
import android.os.Handler;

import com.example.kananyusub.myapplication.models.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recycler_landscapes)
    RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    public static final int PAGE_START = 1;
    private int mCurrentPage = PAGE_START;
    private boolean isLastPage = false;
    private int mTotalPage = 7;
    private boolean isLoading = false;
    int itemCount = 0;
    private List<Person> mItems;
    private CustomRecyclerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mItems = new ArrayList<Person>() {
            {
                add(new Person("Title-" + itemCount, "Desc-" + itemCount));
                add(new Person("Title-" + itemCount, "Desc-" + itemCount));
                add(new Person("Title-" + itemCount, "Desc-" + itemCount, "Gender-" + itemCount));
                add(new Person("Title-" + itemCount, "Desc-" + itemCount));
                add(new Person("Title-" + itemCount, "Desc-" + itemCount, "Gender-" + itemCount));
                add(new Person("Title-" + itemCount, "Desc-" + itemCount));
                add(new Person("Title-" + itemCount, "Desc-" + itemCount));
                add(new Person("Title-" + itemCount, "Desc-" + itemCount, "Gender-" + itemCount));
                add(new Person("Title-" + itemCount, "Desc-" + itemCount));
                add(new Person("Title-" + itemCount, "Desc-" + itemCount, "Gender-" + itemCount));

            }
        };

        mAdapter = new CustomRecyclerAdapter(this, mItems);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(itemDecoration);
        mRecyclerView.setAdapter(mAdapter);

        // pagination
        mRecyclerView.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                mCurrentPage++;
                createNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    public void createNextPage() {
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        List<Person> newPageList = new ArrayList<>();
                        Random r = new Random();
                        for (int i = 0; i < 10; i++) {
                            itemCount++;
                            int randomNumber = r.nextInt(2);
                            Person person;
                            if (randomNumber == 0) {
                                person = new Person("Title-" + itemCount, "Desc-" + itemCount);
                            } else {
                                person = new Person("Title-" + itemCount, "Desc-" + itemCount, "Gender-" + itemCount);
                            }
                            mItems.addAll(newPageList);
                        }

                        if (mCurrentPage != PAGE_START) mAdapter.removeLoading();
                        mAdapter.addAll(newPageList);
                        if (mCurrentPage < mTotalPage) mAdapter.addLoading();
                        isLoading = false;
                    }
                }, 2000);
    }
}
