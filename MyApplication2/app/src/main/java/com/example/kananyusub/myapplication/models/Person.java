package com.example.kananyusub.myapplication.models;

import com.example.kananyusub.myapplication.utils.ImageProvider;

public class Person {
    private String mFullName;
    private String mAge;
    private Integer mImageId;
    private String mGender;

    public Person() {
    }

    public Person(String fullName, String age) {
        mFullName = fullName;
        mAge = age;
        mGender = null;
        mImageId = ImageProvider.getNextPersonId();
    }

    public Person(String fullName, String age, String gender) {
        mFullName = fullName;
        mAge = age;
        mGender = gender;
        mImageId = ImageProvider.getNextPersonId();
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public String getAge() {
        return mAge;
    }

    public void setAge(String age) {
        mAge = age;
    }

    public Integer getImageId() {
        return mImageId;
    }

    public void setImageId(Integer imageId) {
        mImageId = imageId;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public boolean isGenderVisible() {
        return getGender() == null ? false : true;
    }
}
