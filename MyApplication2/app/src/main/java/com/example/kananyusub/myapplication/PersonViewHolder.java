package com.example.kananyusub.myapplication;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PersonViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.image_person)
    ImageView mImageViewPerson;
    @BindView(R.id.text_fullname)
    TextView mTextViewFullName;
    @BindView(R.id.text_age)
    TextView mTextViewAge;

    public PersonViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
