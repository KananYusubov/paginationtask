package com.example.kananyusub.myapplication;

import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FooterHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.progress_bar_loader)
    ProgressBar mProgressBar;

    public FooterHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
